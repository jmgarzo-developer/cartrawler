package com.cartrawler.jmgarzo.cartrawler.Utils;

import android.content.Context;
import android.database.Cursor;

import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class DbUtils {



    public static final String[] VENDOR_COLUMNS = {
            CartrawlerContract.VendorEntry._ID,
            CartrawlerContract.VendorEntry.CODE,
            CartrawlerContract.VendorEntry.NAME,
    };

    public static final int COL_VENDOR_ID = 0;
    public static final int COL_VENDOR_CODE = 1;
    public static final int COL_VENDOR_NAME = 2;


    public static final String[] VEHICLE_COLUMNS = {
            CartrawlerContract.VehicleEntry._ID,
            CartrawlerContract.VehicleEntry.VENDOR,
            CartrawlerContract.VehicleEntry.STATUS,
            CartrawlerContract.VehicleEntry.AIR_CONDITION_IND,
            CartrawlerContract.VehicleEntry.TRANSMISSION_TYPE,
            CartrawlerContract.VehicleEntry.FUEL_TYPE,
            CartrawlerContract.VehicleEntry.DRIVE_TYPE,
            CartrawlerContract.VehicleEntry.PASSENGER_QUANTITY,
            CartrawlerContract.VehicleEntry.BAGGAGE_QUANTITY,
            CartrawlerContract.VehicleEntry.CODE,
            CartrawlerContract.VehicleEntry.CODE_CONTEXT,
            CartrawlerContract.VehicleEntry.DOOR_COUNT,
            CartrawlerContract.VehicleEntry.VEHICLE_MAKE_MODEL,
            CartrawlerContract.VehicleEntry.PICTURE_URL,
            CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT,
            CartrawlerContract.VehicleEntry.ESTIMATED_TOTAL_AMOUNT,
            CartrawlerContract.VehicleEntry.CURRENCY_CODE

    };

    public static final int COL_VEHICLE_ID = 0;
    public static final int COL_VEHICLE_VENDOR = 1;
    public static final int COL_VEHICLE_STATUS = 2;
    public static final int COL_VEHICLE_AIR_CONDITION_IND = 3;
    public static final int COL_VEHICLE_TRANSMISSION_TYPE = 4;
    public static final int COL_VEHICLE_FUEL_TYPE = 5;
    public static final int COL_VEHICLE_DRIVE_TYPE = 6;
    public static final int COL_VEHICLE_PASSENGER_QUANTITY = 7;
    public static final int COL_VEHICLE_BAGGAGE_QUANTITY = 8;
    public static final int COL_VEHICLE_CODE = 9;
    public static final int COL_VEHICLE_CODE_CONTEXT = 10;
    public static final int COL_VEHICLE_DOOR_COUNT = 11;
    public static final int COL_VEHICLE_VEHICLE_MAKE_MODEL = 12;
    public static final int COL_VEHICLE_PICTURE_URL = 13;
    public static final int COL_VEHICLE_RATE_TOTAL_AMOUNT = 14;
    public static final int COL_VEHICLE_ESTIMATED_TOTAL_AMOUNT = 15;
    public static final int COL_VEHICLE_CURRENCY_CODE = 16;


    public static final String[] VEHICLE_VENDOR_COLUMNS = {
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry._ID,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.STATUS,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.AIR_CONDITION_IND,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.TRANSMISSION_TYPE,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.FUEL_TYPE,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.DRIVE_TYPE,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.PASSENGER_QUANTITY,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.BAGGAGE_QUANTITY,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.CODE,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.CODE_CONTEXT,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.DOOR_COUNT,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.VEHICLE_MAKE_MODEL,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.PICTURE_URL,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.ESTIMATED_TOTAL_AMOUNT,
            CartrawlerContract.VehicleEntry.TABLE_NAME + "." +CartrawlerContract.VehicleEntry.CURRENCY_CODE,
            CartrawlerContract.VendorEntry.TABLE_NAME + "." +CartrawlerContract.VendorEntry.NAME,
            CartrawlerContract.VendorEntry.TABLE_NAME + "." +CartrawlerContract.VendorEntry.CODE
    };

    public static final int COL_VEHICLE_VENDOR_ID = 0;
    public static final int COL_VEHICLE_VENDOR_STATUS = 1;
    public static final int COL_VEHICLE_VENDOR_AIR_CONDITION_IND = 2;
    public static final int COL_VEHICLE_VENDOR_TRANSMISSION_TYPE = 3;
    public static final int COL_VEHICLE_VENDOR_FUEL_TYPE = 4;
    public static final int COL_VEHICLE_VENDOR_DRIVE_TYPE = 5;
    public static final int COL_VEHICLE_VENDOR_PASSENGER_QUANTITY = 6;
    public static final int COL_VEHICLE_VENDOR_BAGGAGE_QUANTITY = 7;
    public static final int COL_VEHICLE_VENDOR_VEHICLE_CODE = 8;
    public static final int COL_VEHICLE_VENDOR_CODE_CONTEXT = 9;
    public static final int COL_VEHICLE_VENDOR_DOOR_COUNT = 10;
    public static final int COL_VEHICLE_VENDOR_VEHICLE_MAKE_MODEL = 11;
    public static final int COL_VEHICLE_VENDOR_PICTURE_URL = 12;
    public static final int COL_VEHICLE_VENDOR_RATE_TOTAL_AMOUNT = 13;
    public static final int COL_VEHICLE_VENDOR_ESTIMATED_TOTAL_AMOUNT = 14;
    public static final int COL_VEHICLE_VENDOR_CURRENCY_CODE = 15;
    public static final int COL_VEHICLE_VENDOR_NAME = 16;
    public static final int COL_VEHICLE_VENDOR_VENDOR_CODE = 17;



    public static final String[] RENTAL_CORE_COLUMNS = {
            CartrawlerContract.RentalCoreEntry._ID,
            CartrawlerContract.RentalCoreEntry.PICK_UP_DATE_TIME,
            CartrawlerContract.RentalCoreEntry.RETURN_DATE_TIME,
            CartrawlerContract.RentalCoreEntry.PICK_UP_LOCATION,
            CartrawlerContract.RentalCoreEntry.RETURN_LOCATION
    };

    public static final int COL_RENTAL_CORE_ID = 0;
    public static final int COL_RENTAL_CORE_PICK_UP_DATE_TIME = 1;
    public static final int COL_RENTAL_CORE_RETURN_DATE_TIME = 2;
    public static final int COL_RENTAL_CORE_PICK_UP_LOCATION = 3;
    public static final int COL_RENTAL_CORE_RETURN_LOCATION = 4;


    public static long getDbVendorId(Context context, String vendorCode) {
        Long idVendor = null;
        Cursor cursor = context.getContentResolver().query(
                CartrawlerContract.VendorEntry.CONTENT_URI,
                DbUtils.VENDOR_COLUMNS,
                CartrawlerContract.VendorEntry.CODE + " = ? ",
                new String[]{vendorCode},
                null);
        if (cursor != null && cursor.moveToFirst()) {
            idVendor = cursor.getLong(DbUtils.COL_VENDOR_ID);
        }
        return idVendor;
    }

}
