package com.cartrawler.jmgarzo.cartrawler;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;
import com.cartrawler.jmgarzo.cartrawler.model.VehicleVendor;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleAdapterViewHolder> {

    private Cursor mCursor;
    private Context mContext;
    private final VehicleAdapterOnClickHandler mClickHandler;


    public interface VehicleAdapterOnClickHandler {
        void onClick(VehicleVendor vehicleVendor);
    }

    public VehicleAdapter(Context context, VehicleAdapterOnClickHandler clickHandler) {
        mContext = context;
        mClickHandler = clickHandler;
    }

    @Override
    public VehicleAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_list_vehicles, parent, false);
        view.setFocusable(true);
        return new VehicleAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VehicleAdapterViewHolder holder, int position) {

        mCursor.moveToPosition(position);

        if (mCursor != null && mCursor.moveToPosition(position)) {

            String pictureUrl = mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_PICTURE_URL);
            Glide.with(mContext).load(pictureUrl).into(holder.mImageView);
            holder.mVendor.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_NAME));
            holder.mStatus.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_STATUS));

            holder.mMakeModel.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_VEHICLE_MAKE_MODEL));
            holder.mFuelType.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_FUEL_TYPE));
            holder.mTransmision.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_TRANSMISSION_TYPE));
            holder.mDoors.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_DOOR_COUNT));
            holder.mPassenger.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_PASSENGER_QUANTITY));
            holder.mBaggage.setText(mCursor.getString(DbUtils.COL_VEHICLE_VENDOR_BAGGAGE_QUANTITY));
            holder.mPrice.setText(Double.toString(mCursor.getDouble(DbUtils.COL_VEHICLE_VENDOR_RATE_TOTAL_AMOUNT)));


        }

    }

    @Override
    public int getItemCount() {
        if (null == mCursor) return 0;
        return mCursor.getCount();
    }

    void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }

    public class VehicleAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView mImageView;
        private final TextView mVendor;
        private final TextView mStatus;
        private final TextView mMakeModel;
        private final TextView mFuelType;
        private final TextView mTransmision;
        private final TextView mPassenger;
        private final TextView mBaggage;
        private final TextView mDoors;
        private final TextView mPrice;

        public VehicleAdapterViewHolder(View view) {
            super(view);
            mImageView = view.findViewById(R.id.list_car_image);
            mVendor = view.findViewById(R.id.list_tv_vendor);
            mStatus = view.findViewById(R.id.list_tv_status);
            mMakeModel = view.findViewById(R.id.list_tv_make_model);
            mFuelType = view.findViewById(R.id.list_tv_fuel_type);
            mTransmision = view.findViewById(R.id.list_tv_transmission);
            mPassenger = view.findViewById(R.id.list_tv_passenger);
            mBaggage = view.findViewById(R.id.list_tv_baggage);
            mDoors = view.findViewById(R.id.list_tv_doors);
            mPrice = view.findViewById(R.id.list_tv_price);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            VehicleVendor vehicleVendor  = new VehicleVendor ();
            vehicleVendor.cursorToVehicleVendor(mCursor,adapterPosition);
            mClickHandler.onClick(vehicleVendor);
        }
    }
}
