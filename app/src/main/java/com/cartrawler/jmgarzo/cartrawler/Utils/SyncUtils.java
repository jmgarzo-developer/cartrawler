package com.cartrawler.jmgarzo.cartrawler.Utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;
import com.cartrawler.jmgarzo.cartrawler.sync.CartrawlerJobService;
import com.cartrawler.jmgarzo.cartrawler.sync.CartrawlerSyncDbService;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class SyncUtils {

    private static boolean sInitialized;

    private static final int SYNC_INTERVAL_HOURS = 3;
    private static final int SYNC_INTERVAL_SECONDS = (int) java.util.concurrent.TimeUnit.HOURS.toSeconds(SYNC_INTERVAL_HOURS);
    private static final int SYNC_FLEXTIME_SECONDS = SYNC_INTERVAL_SECONDS / 3;

    private static final String CAR_SYNC_TAG = "car-sync";


    static void scheduleFirebaseJobDispatcherSync(@NonNull final Context context) {
        GooglePlayDriver driver = new GooglePlayDriver(context);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(driver);

        Job syncMoviesJob = dispatcher.newJobBuilder()
                .setService(CartrawlerJobService.class)
                .setTag(CAR_SYNC_TAG)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setLifetime(Lifetime.FOREVER)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(
                        SYNC_INTERVAL_SECONDS,
                        SYNC_INTERVAL_SECONDS + SYNC_FLEXTIME_SECONDS))
                .setReplaceCurrent(true)
                .build();

        dispatcher.schedule(syncMoviesJob);
    }

    synchronized public static void initialize(@NonNull final Context context) {
        if (sInitialized) return;
        sInitialized = true;
        scheduleFirebaseJobDispatcherSync(context);


        Thread checkForEmpty = new Thread(new Runnable() {
            @Override
            public void run() {

                Cursor cursor = context.getContentResolver().query(
                        CartrawlerContract.VehicleEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                );

                if (cursor == null || cursor.getCount() == 0) {
                    startImmediateSync(context);
                }
                cursor.close();
            }
        });
        checkForEmpty.start();
    }

    public static void startImmediateSync(@NonNull final Context context) {
        Intent intentToSyncImmediately = new Intent(context, CartrawlerSyncDbService.class);
        context.startService(intentToSyncImmediately);
    }
}
