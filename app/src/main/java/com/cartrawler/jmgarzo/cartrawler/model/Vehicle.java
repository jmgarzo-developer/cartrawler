package com.cartrawler.jmgarzo.cartrawler.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;
import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class Vehicle implements Parcelable {
    private long id;
    private long vendor;
    private String status;
    private String airConditionInd;
    private String transmissionType;
    private String fuelType;
    private String driveType;
    private String passengerQuantity;
    private String baggageQuantity;
    private String code;
    private String codeContext;
    private String doorCount;
    private String vehicleMakeModel;
    private String pictureUrl;
    private Double rateTotalAmount;
    private Double estimatedTotalAmount;
    private String currencyCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVendor() {
        return vendor;
    }

    public void setVendor(long vendor) {
        this.vendor = vendor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAirConditionInd() {
        return airConditionInd;
    }

    public void setAirConditionInd(String airConditionInd) {
        this.airConditionInd = airConditionInd;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getPassengerQuantity() {
        return passengerQuantity;
    }

    public void setPassengerQuantity(String passengerQuantity) {
        this.passengerQuantity = passengerQuantity;
    }

    public String getBaggageQuantity() {
        return baggageQuantity;
    }

    public void setBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeContext() {
        return codeContext;
    }

    public void setCodeContext(String codeContext) {
        this.codeContext = codeContext;
    }

    public String getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(String doorCount) {
        this.doorCount = doorCount;
    }

    public String getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    public void setVehicleMakeModel(String vehicleMakeModel) {
        this.vehicleMakeModel = vehicleMakeModel;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Double getRateTotalAmount() {
        return rateTotalAmount;
    }

    public void setRateTotalAmount(Double rateTotalAmount) {
        this.rateTotalAmount = rateTotalAmount;
    }

    public Double getEstimatedTotalAmount() {
        return estimatedTotalAmount;
    }

    public void setEstimatedTotalAmount(Double estimatedTotalAmount) {
        this.estimatedTotalAmount = estimatedTotalAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(CartrawlerContract.VehicleEntry.VENDOR,getVendor());
        contentValues.put(CartrawlerContract.VehicleEntry.STATUS,getStatus());
        contentValues.put(CartrawlerContract.VehicleEntry.AIR_CONDITION_IND,getAirConditionInd());
        contentValues.put(CartrawlerContract.VehicleEntry.TRANSMISSION_TYPE, getTransmissionType());
        contentValues.put(CartrawlerContract.VehicleEntry.FUEL_TYPE,getFuelType());
        contentValues.put(CartrawlerContract.VehicleEntry.DRIVE_TYPE,getDriveType());
        contentValues.put(CartrawlerContract.VehicleEntry.PASSENGER_QUANTITY,getPassengerQuantity());
        contentValues.put(CartrawlerContract.VehicleEntry.BAGGAGE_QUANTITY,getBaggageQuantity());
        contentValues.put(CartrawlerContract.VehicleEntry.CODE,getCode());
        contentValues.put(CartrawlerContract.VehicleEntry.CODE_CONTEXT,getCodeContext());
        contentValues.put(CartrawlerContract.VehicleEntry.DOOR_COUNT,getDoorCount());
        contentValues.put(CartrawlerContract.VehicleEntry.VEHICLE_MAKE_MODEL,getVehicleMakeModel());
        contentValues.put(CartrawlerContract.VehicleEntry.PICTURE_URL,getPictureUrl());
        contentValues.put(CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT,getRateTotalAmount());
        contentValues.put(CartrawlerContract.VehicleEntry.ESTIMATED_TOTAL_AMOUNT,getEstimatedTotalAmount());
        contentValues.put(CartrawlerContract.VehicleEntry.CURRENCY_CODE,getCurrencyCode());

        return contentValues;

    }

    public void cursorToVehicle(Cursor cursor){

        id = cursor.getLong(DbUtils.COL_VEHICLE_ID);
        vendor = cursor.getLong(DbUtils.COL_VEHICLE_VENDOR);
        status = cursor.getString(DbUtils.COL_VEHICLE_STATUS);
        airConditionInd = cursor.getString(DbUtils.COL_VEHICLE_AIR_CONDITION_IND);
        transmissionType = cursor.getString(DbUtils.COL_VEHICLE_TRANSMISSION_TYPE);
        fuelType = cursor.getString(DbUtils.COL_VEHICLE_FUEL_TYPE);
        driveType = cursor.getString(DbUtils.COL_VEHICLE_DRIVE_TYPE);
        passengerQuantity = cursor.getString(DbUtils.COL_VEHICLE_PASSENGER_QUANTITY);
        baggageQuantity = cursor.getString(DbUtils.COL_VEHICLE_BAGGAGE_QUANTITY);
        code = cursor.getString(DbUtils.COL_VEHICLE_CODE);
        codeContext = cursor.getString(DbUtils.COL_VEHICLE_CODE_CONTEXT);
        doorCount = cursor.getString(DbUtils.COL_VEHICLE_DOOR_COUNT);
        vehicleMakeModel = cursor.getString(DbUtils.COL_VEHICLE_VEHICLE_MAKE_MODEL);
        pictureUrl = cursor.getString(DbUtils.COL_VEHICLE_PICTURE_URL);
        rateTotalAmount = cursor.getDouble(DbUtils.COL_VEHICLE_RATE_TOTAL_AMOUNT);
        estimatedTotalAmount = cursor.getDouble(DbUtils.COL_VEHICLE_ESTIMATED_TOTAL_AMOUNT);
        currencyCode = cursor.getString(DbUtils.COL_VEHICLE_CURRENCY_CODE);

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.vendor);
        dest.writeString(this.status);
        dest.writeString(this.airConditionInd);
        dest.writeString(this.transmissionType);
        dest.writeString(this.fuelType);
        dest.writeString(this.driveType);
        dest.writeString(this.passengerQuantity);
        dest.writeString(this.baggageQuantity);
        dest.writeString(this.code);
        dest.writeString(this.codeContext);
        dest.writeString(this.doorCount);
        dest.writeString(this.vehicleMakeModel);
        dest.writeString(this.pictureUrl);
        dest.writeValue(this.rateTotalAmount);
        dest.writeValue(this.estimatedTotalAmount);
        dest.writeString(this.currencyCode);
    }

    public Vehicle() {
    }

    protected Vehicle(Parcel in) {
        this.id = in.readLong();
        this.vendor = in.readLong();
        this.status = in.readString();
        this.airConditionInd = in.readString();
        this.transmissionType = in.readString();
        this.fuelType = in.readString();
        this.driveType = in.readString();
        this.passengerQuantity = in.readString();
        this.baggageQuantity = in.readString();
        this.code = in.readString();
        this.codeContext = in.readString();
        this.doorCount = in.readString();
        this.vehicleMakeModel = in.readString();
        this.pictureUrl = in.readString();
        this.rateTotalAmount = (Double) in.readValue(Double.class.getClassLoader());
        this.estimatedTotalAmount = (Double) in.readValue(Double.class.getClassLoader());
        this.currencyCode = in.readString();
    }

    public static final Parcelable.Creator<Vehicle> CREATOR = new Parcelable.Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel source) {
            return new Vehicle(source);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}
