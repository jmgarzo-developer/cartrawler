package com.cartrawler.jmgarzo.cartrawler.Utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.cartrawler.jmgarzo.cartrawler.model.RentalCore;
import com.cartrawler.jmgarzo.cartrawler.model.Vehicle;
import com.cartrawler.jmgarzo.cartrawler.model.Vendor;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class NetworkUtils {

    private static final String LOG_TAG = NetworkUtils.class.getSimpleName();

    private static final String BASE_URL = "http://www.cartrawler.com/ctabe/";
    private static final String CARS = "cars.json";


    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    public static ArrayList<Vendor> getVendors(Context context) {
        String response = "";
        ArrayList<Vendor> vendorsList = null;

        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendEncodedPath(CARS)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            response = getResponseFromHttpUrl(url);
            vendorsList = JsonUtils.getVendorsFromJson(response);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.toString());
            if (response.equalsIgnoreCase("")) {
                //TODO: Server down
            }
        }
        return vendorsList;
    }


    public static ArrayList<RentalCore> getRentalCore(Context context) {
        String response = "";
        ArrayList<RentalCore> rentalCoreList = null;

        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendEncodedPath(CARS)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            response = getResponseFromHttpUrl(url);
            rentalCoreList = JsonUtils.getRentalCoreFromJson(response);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.toString());
            if (response.equalsIgnoreCase("")) {
                //TODO: Server down
            }
        }
        return rentalCoreList;
    }


    public static ArrayList<Vehicle> getVehicles(Context context) {
        String response = "";
        ArrayList<Vehicle> vehiclesList = null;

        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendEncodedPath(CARS)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            response = getResponseFromHttpUrl(url);
            vehiclesList = JsonUtils.getVehiclesFromJson(context,response);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.toString());
            if (response.equalsIgnoreCase("")) {
                //TODO: Server down
            }
        }
        return vehiclesList;
    }



}
