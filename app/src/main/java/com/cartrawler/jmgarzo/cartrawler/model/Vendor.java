package com.cartrawler.jmgarzo.cartrawler.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;
import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class Vendor implements Parcelable {

    private long id;
    private String code;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();

        contentValues.put(CartrawlerContract.VendorEntry.CODE,getCode());
        contentValues.put(CartrawlerContract.VendorEntry.NAME,getName());

        return contentValues;
    }

    public void cursorToVendor(Cursor cursor){
        id = cursor.getLong(DbUtils.COL_VENDOR_ID);
        code = cursor.getString(DbUtils.COL_VENDOR_CODE);
        name = cursor.getString(DbUtils.COL_VENDOR_NAME);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.code);
        dest.writeString(this.name);
    }

    public Vendor() {
    }

    protected Vendor(Parcel in) {
        this.id = in.readLong();
        this.code = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Vendor> CREATOR = new Parcelable.Creator<Vendor>() {
        @Override
        public Vendor createFromParcel(Parcel source) {
            return new Vendor(source);
        }

        @Override
        public Vendor[] newArray(int size) {
            return new Vendor[size];
        }
    };
}
