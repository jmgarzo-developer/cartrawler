package com.cartrawler.jmgarzo.cartrawler.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by jmgarzo on 11/2/2017.
 */

public class CartrawlerProvider extends ContentProvider {

    private final String LOG_TAG = CartrawlerProvider.class.getSimpleName();

    static final int VENDOR = 100;

    static final int VEHICLE = 200;
    static final int VEHICLE_WITH_ID = 201;

    static final int RENTAL_CORE = 300;

    static final int VEHICLE_VENDOR = 400;


    private static final SQLiteQueryBuilder sVendorByVehicleQueryBuilder;

    static {
        sVendorByVehicleQueryBuilder = new SQLiteQueryBuilder();
        sVendorByVehicleQueryBuilder.setTables(
                CartrawlerContract.VehicleEntry.TABLE_NAME + " INNER JOIN " +
                        CartrawlerContract.VendorEntry.TABLE_NAME +
                        " ON " + CartrawlerContract.VehicleEntry.TABLE_NAME +
                        "." + CartrawlerContract.VehicleEntry.VENDOR +
                        " = " + CartrawlerContract.VendorEntry.TABLE_NAME +
                        "." + CartrawlerContract.VendorEntry._ID);
    }

    private Cursor getVehiclesWithVendor(
            Uri uri, String[] projection, String selection, String [] selectionArgs,String groupBy, String having, String sortOrder) {
        return sVendorByVehicleQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                groupBy,
                having,
                sortOrder
        );
    }


    private CartrawlerDBHelper mOpenHelper;

    private static final UriMatcher sUriMatcher = buildUriMatcher();


    static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CartrawlerContract.CONTENT_AUTHORITY, CartrawlerContract.PATH_VENDOR, VENDOR);

        matcher.addURI(CartrawlerContract.CONTENT_AUTHORITY, CartrawlerContract.PATH_VEHICLE, VEHICLE);
        matcher.addURI(CartrawlerContract.CONTENT_AUTHORITY, CartrawlerContract.PATH_VEHICLE + "/*", VEHICLE_WITH_ID);

        matcher.addURI(CartrawlerContract.CONTENT_AUTHORITY, CartrawlerContract.PATH_RENTAL_CORE, RENTAL_CORE);

        matcher.addURI(CartrawlerContract.CONTENT_AUTHORITY, CartrawlerContract.PATH_VEHICLE_VENDOR, VEHICLE_VENDOR);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new CartrawlerDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor returnCursor;
        switch (sUriMatcher.match(uri)) {
            case VENDOR: {
                returnCursor = mOpenHelper.getReadableDatabase().query(
                        CartrawlerContract.VendorEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case VEHICLE: {
                returnCursor = mOpenHelper.getReadableDatabase().query(
                        CartrawlerContract.VehicleEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case VEHICLE_WITH_ID: {
                returnCursor = mOpenHelper.getReadableDatabase().query(
                        CartrawlerContract.VehicleEntry.TABLE_NAME,
                        projection,
                        CartrawlerContract.VehicleEntry._ID + " = ?",
                        new String[]{uri.getPathSegments().get(1)},
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case RENTAL_CORE: {
                returnCursor = mOpenHelper.getReadableDatabase().query(
                        CartrawlerContract.RentalCoreEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case VEHICLE_VENDOR: {
                returnCursor = getVehiclesWithVendor(uri, projection, selection,selectionArgs,null,null,  sortOrder);
                break;
            }

            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        if(uri.toString().equalsIgnoreCase(CartrawlerContract.VehicleVendorEntry.CONTENT_URI.toString())){
            returnCursor.setNotificationUri(getContext().getContentResolver(), CartrawlerContract.VehicleEntry.CONTENT_URI);
        }else {
            returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return returnCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        switch (sUriMatcher.match(uri)) {
            case VENDOR:
                return CartrawlerContract.VendorEntry.CONTENT_DIR_TYPE;
            case VEHICLE:
                return CartrawlerContract.VehicleEntry.CONTENT_DIR_TYPE;
            case VEHICLE_WITH_ID:
                return CartrawlerContract.VehicleEntry.CONTENT_ITEM_TYPE;
            case RENTAL_CORE:
                return CartrawlerContract.RentalCoreEntry.CONTENT_DIR_TYPE;
            case VEHICLE_VENDOR:
                return CartrawlerContract.VehicleEntry.CONTENT_DIR_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        Uri returnUri = null;
        switch (sUriMatcher.match(uri)) {
            case VENDOR: {
                long id = db.insert(CartrawlerContract.VendorEntry.TABLE_NAME,
                        null,
                        contentValues);
                if (id > 0) {
                    returnUri = ContentUris.withAppendedId(CartrawlerContract.VendorEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;
            }
            case VEHICLE: {
                long id = db.insert(CartrawlerContract.VehicleEntry.TABLE_NAME,
                        null,
                        contentValues);
                if (id > 0) {
                    returnUri = ContentUris.withAppendedId(CartrawlerContract.VehicleEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;
            }

            case RENTAL_CORE: {
                long id = db.insert(CartrawlerContract.RentalCoreEntry.TABLE_NAME,
                        null,
                        contentValues);
                if (id > 0) {
                    returnUri = ContentUris.withAppendedId(CartrawlerContract.RentalCoreEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;
            }

            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[]
            selectionArgs) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int numDeleted = 0;
        switch (sUriMatcher.match(uri)) {
            case VENDOR: {
                numDeleted = db.delete(
                        CartrawlerContract.VendorEntry.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;
            }

            case VEHICLE: {
                numDeleted = db.delete(
                        CartrawlerContract.VehicleEntry.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;
            }

            case VEHICLE_WITH_ID: {
                numDeleted = db.delete(CartrawlerContract.VehicleEntry.TABLE_NAME,
                        CartrawlerContract.VehicleEntry._ID + " = ?",
                        new String[]{uri.getPathSegments().get(1)});
                break;
            }

            case RENTAL_CORE: {
                numDeleted = db.delete(
                        CartrawlerContract.RentalCoreEntry.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return numDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String
            selection, @Nullable String[] selectionArgs) {
        if (values == null) {
            throw new IllegalArgumentException("Cannot have null content values");
        }
        int numUpdates = 0;

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case VENDOR: {
                numUpdates = db.update(CartrawlerContract.VendorEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            }

            case VEHICLE: {
                numUpdates = db.update(CartrawlerContract.VehicleEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            }

            case VEHICLE_WITH_ID: {
                numUpdates = db.update(CartrawlerContract.VehicleEntry.TABLE_NAME,
                        values,
                        CartrawlerContract.VehicleEntry._ID + " = ?",
                        new String[]{uri.getPathSegments().get(1)});
                break;
            }

            case RENTAL_CORE: {
                numUpdates = db.update(CartrawlerContract.RentalCoreEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return numUpdates;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int numInserted = 0;

        switch (sUriMatcher.match(uri)) {
            case VENDOR: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        if (value == null) {
                            throw new IllegalArgumentException("Cannot have null content values");
                        }
                        long id = db.insert(CartrawlerContract.VendorEntry.TABLE_NAME, null, value);
                        if (id != -1) {
                            numInserted++;
                        }
                    }
                    db.setTransactionSuccessful();

                } finally {
                    db.endTransaction();
                }
                break;
            }
            case VEHICLE: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        if (value == null) {
                            throw new IllegalArgumentException("Cannot have null content values");
                        }
                        long id = db.insert(CartrawlerContract.VehicleEntry.TABLE_NAME, null, value);
                        if (id != -1) {
                            numInserted++;
                        }
                    }
                    db.setTransactionSuccessful();

                } finally {
                    db.endTransaction();
                }
                break;
            }

            case RENTAL_CORE: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        if (value == null) {
                            throw new IllegalArgumentException("Cannot have null content values");
                        }
                        long id = db.insert(CartrawlerContract.RentalCoreEntry.TABLE_NAME, null, value);
                        if (id != -1) {
                            numInserted++;
                        }
                    }
                    db.setTransactionSuccessful();

                } finally {
                    db.endTransaction();
                }
                break;
            }
            default:
                return super.bulkInsert(uri, values);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return numInserted;
    }
}
