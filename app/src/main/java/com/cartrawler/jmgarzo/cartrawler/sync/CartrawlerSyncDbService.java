package com.cartrawler.jmgarzo.cartrawler.sync;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class CartrawlerSyncDbService extends IntentService {

    public CartrawlerSyncDbService() {
        super("CartrawlerSyncDbService");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        SyncTasks.syncDb(this);

    }
}
