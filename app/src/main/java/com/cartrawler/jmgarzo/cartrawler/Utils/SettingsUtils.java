package com.cartrawler.jmgarzo.cartrawler.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cartrawler.jmgarzo.cartrawler.R;

/**
 * Created by jmgarzo on 11/4/2017.
 */

public class SettingsUtils {

    private final String LOG_TAG = SettingsUtils.class.getSimpleName();

    public static String getPreferredSortBy(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(context.getString(R.string.pref_sort_by_key),context.getString(R.string.pref_sort_by_default));
    }

    public static boolean isPreferenceSortByPriceAscending(Context context){
        if(getPreferredSortBy(context).equalsIgnoreCase(context.getString(R.string.pref_sort_by_value_price_ascending))){
            return true;
        }
        return false;
    }

    public static boolean isPreferenceSortByPriceDescending(Context context){
        if(getPreferredSortBy(context).equalsIgnoreCase(context.getString(R.string.pref_sort_by_value__price_descending))){
            return true;
        }
        return false;
    }

    public static boolean isPreferenceSortByVendor(Context context){
        if(getPreferredSortBy(context).equalsIgnoreCase(context.getString(R.string.pref_sort_by_value_vendor))){
            return true;
        }
        return false;
    }

}
