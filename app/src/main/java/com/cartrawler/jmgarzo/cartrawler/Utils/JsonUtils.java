package com.cartrawler.jmgarzo.cartrawler.Utils;

import android.content.Context;
import android.util.Log;

import com.cartrawler.jmgarzo.cartrawler.model.RentalCore;
import com.cartrawler.jmgarzo.cartrawler.model.Vehicle;
import com.cartrawler.jmgarzo.cartrawler.model.Vendor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils.getDbVendorId;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class JsonUtils {

    private static final String LOG_TAG = JsonUtils.class.getSimpleName();

    private static final String VEH_AVAIL_RS_CORE = "VehAvailRSCore";
    private static final String VEH_VENDOR_AVAILS = "VehVendorAvails";
    private static final String VEH_RENTAL_CORE = "VehRentalCore";

    private static final String VEH_AVAILS = "VehAvails";


    //VENDOR JSON FIELDS
    private static final String VENDOR = "Vendor";
    private static final String VENDOR_CODE = "@Code";
    private static final String VENDOR_NAME = "@Name";

    //RENTAL CORE FIELDS
    private static final String RENTAL_CORE_PICK_UP_DATE_TIME = "@PickUpDateTime";
    private static final String RENTAL_CORE_RETURN_DATE_TIME = "@ReturnDateTime";
    private static final String RENTAL_CORE_PICK_UP_LOCATION = "PickUpLocation";
    private static final String RENTAL_CORE_RETURN_LOCATION = "ReturnLocation";
    private static final String RENTAL_CORE_NAME = "@Name";

    //VEHICLE FIELDS
    private static final String VEHICLE_STATUS = "@Status";
    private static final String VEHICLE_VEHICLE = "Vehicle";
    private static final String VEHICLE_AIR_CONDITION_IND = "@AirConditionInd";
    private static final String VEHICLE_TRANSMISSION_TYPE = "@TransmissionType";
    private static final String VEHICLE_FUEL_TYPE = "@FuelType";
    private static final String VEHICLE_DRIVE_TYPE = "@DriveType";
    private static final String VEHICLE_PASSENGER_QUANTITY = "@PassengerQuantity";
    private static final String VEHICLE_BAGGAGE_QUANTITY = "@BaggageQuantity";
    private static final String VEHICLE_CODE = "@Code";
    private static final String VEHICLE_CODE_CONTEXT = "@CodeContext";
    private static final String VEHICLE_DOOR_COUNT = "@DoorCount";
    private static final String VEHICLE_MAKE_MODEL = "VehMakeModel";
    private static final String VEHICLE_NAME = "@Name";
    private static final String VEHICLE_PICTURE_URL = "PictureURL";
    private static final String VEHICLE_TOTAL_CHARGE = "TotalCharge";
    private static final String VEHICLE_RATE_TOTAL_AMOUNT = "@RateTotalAmount";
    private static final String VEHICLE_ESTIMATED_TOTAL_AMOUNT = "@EstimatedTotalAmount";
    private static final String VEHICLE_CURRENCY_CODE = "@CurrencyCode";



    public static ArrayList<Vendor> getVendorsFromJson(String jsonStr) {
        ArrayList<Vendor> vendorList = null;
        JSONArray carsJsonArray = null;
        JSONObject vehAvailRSCore = null;
        JSONArray vehVendorAvailsList = null;
        try {
            carsJsonArray = new JSONArray(jsonStr);
            vendorList = new ArrayList<>();
            for (int i = 0; i < carsJsonArray.length(); i++) {
                vehAvailRSCore = carsJsonArray.getJSONObject(i).getJSONObject(VEH_AVAIL_RS_CORE);
                vehVendorAvailsList = vehAvailRSCore.getJSONArray(VEH_VENDOR_AVAILS);
                for (int j = 0; j < vehVendorAvailsList.length(); j++) {
                    JSONObject jsonVendorAvails = vehVendorAvailsList.getJSONObject(j);
                    JSONObject vendorJson = jsonVendorAvails.getJSONObject(VENDOR);
                    Vendor vendor = new Vendor();
                    vendor.setCode(vendorJson.getString(VENDOR_CODE));
                    vendor.setName(vendorJson.getString(VENDOR_NAME));
                    vendorList.add(vendor);
                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.toString());
        }
        return vendorList;
    }


    public static ArrayList<RentalCore> getRentalCoreFromJson(String jsonStr) {
        ArrayList<RentalCore> rentalCoreList = null;
        JSONArray carsJsonArray = null;
        JSONObject vehAvailRSCore = null;
        JSONObject vehRentalCore = null;
        try {
            carsJsonArray = new JSONArray(jsonStr);
            rentalCoreList = new ArrayList<>();
            for (int i = 0; i < carsJsonArray.length(); i++) {
                vehAvailRSCore = carsJsonArray.getJSONObject(i).getJSONObject(VEH_AVAIL_RS_CORE);
                vehRentalCore = vehAvailRSCore.getJSONObject(VEH_RENTAL_CORE);

                RentalCore rentalCore = new RentalCore();
                rentalCore.setPickUpDateTime(vehRentalCore.getString(RENTAL_CORE_PICK_UP_DATE_TIME));
                rentalCore.setReturnDateTime(vehRentalCore.getString(RENTAL_CORE_RETURN_DATE_TIME));
                rentalCore.setPickUpLocation(vehRentalCore.getJSONObject(RENTAL_CORE_PICK_UP_LOCATION).getString(RENTAL_CORE_NAME));
                rentalCore.setReturnLocation(vehRentalCore.getJSONObject(RENTAL_CORE_RETURN_LOCATION).getString(RENTAL_CORE_NAME));
                rentalCoreList.add(rentalCore);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.toString());
        }
        return rentalCoreList;
    }

    public static ArrayList<Vehicle> getVehiclesFromJson(Context context, String jsonStr) {
        ArrayList<Vehicle> vehiclesList = null;

        JSONArray carsJsonArray = null;
        JSONObject vehAvailRSCore = null;
        JSONArray vehVendorAvailsArray = null;

        try {
            carsJsonArray = new JSONArray(jsonStr);
            vehiclesList = new ArrayList<>();

            for (int i = 0; i < carsJsonArray.length(); i++) {
                vehAvailRSCore = carsJsonArray.getJSONObject(i).getJSONObject(VEH_AVAIL_RS_CORE);
                vehVendorAvailsArray = vehAvailRSCore.getJSONArray(VEH_VENDOR_AVAILS);
                for (int j = 0; j < vehVendorAvailsArray.length(); j++) {
                    JSONObject jsonVendorAvails = vehVendorAvailsArray.getJSONObject(j);
                    JSONObject jsonVendor = jsonVendorAvails.getJSONObject(VENDOR);
                    String vendorCode = jsonVendor.getString(VENDOR_CODE);
                    Long vendorId = getDbVendorId(context,vendorCode);


                    JSONArray vehAvailsArray = jsonVendorAvails.getJSONArray(VEH_AVAILS);
                    for (int k = 0; k < vehAvailsArray.length(); k++){
                        JSONObject vehAvails = vehAvailsArray.getJSONObject(k);
                        JSONObject vehicleJson = vehAvails.getJSONObject(VEHICLE_VEHICLE);
                        Vehicle vehicle = new Vehicle();

                        if(vendorId != null){
                            vehicle.setVendor(vendorId);
                        }
                        vehicle.setStatus(vehAvails.getString(VEHICLE_STATUS));
                        vehicle.setAirConditionInd(vehicleJson.getString(VEHICLE_AIR_CONDITION_IND));
                        vehicle.setTransmissionType(vehicleJson.getString(VEHICLE_TRANSMISSION_TYPE));
                        vehicle.setFuelType(vehicleJson.getString(VEHICLE_FUEL_TYPE));
                        vehicle.setDriveType(vehicleJson.getString(VEHICLE_DRIVE_TYPE));
                        vehicle.setPassengerQuantity(vehicleJson.getString(VEHICLE_PASSENGER_QUANTITY));
                        vehicle.setBaggageQuantity(vehicleJson.getString(VEHICLE_BAGGAGE_QUANTITY));
                        vehicle.setCode(vehicleJson.getString(VEHICLE_CODE));
                        vehicle.setCodeContext(vehicleJson.getString(VEHICLE_CODE_CONTEXT));
                        vehicle.setDoorCount(vehicleJson.getString(VEHICLE_DOOR_COUNT));
                        vehicle.setVehicleMakeModel(vehicleJson.getJSONObject(VEHICLE_MAKE_MODEL).getString(VEHICLE_NAME));
                        vehicle.setPictureUrl(vehicleJson.getString(VEHICLE_PICTURE_URL));
                        vehicle.setRateTotalAmount(vehAvails.getJSONObject(VEHICLE_TOTAL_CHARGE).getDouble(VEHICLE_RATE_TOTAL_AMOUNT));
                        vehicle.setEstimatedTotalAmount(vehAvails.getJSONObject(VEHICLE_TOTAL_CHARGE).getDouble(VEHICLE_ESTIMATED_TOTAL_AMOUNT));
                        vehicle.setCurrencyCode(vehAvails.getJSONObject(VEHICLE_TOTAL_CHARGE).getString(VEHICLE_CURRENCY_CODE));

                        vehiclesList.add(vehicle);
                    }

                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.toString());
        }
        return vehiclesList;
    }

}
