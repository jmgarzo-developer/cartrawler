package com.cartrawler.jmgarzo.cartrawler.sync;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.cartrawler.jmgarzo.cartrawler.Utils.NetworkUtils;
import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;
import com.cartrawler.jmgarzo.cartrawler.model.RentalCore;
import com.cartrawler.jmgarzo.cartrawler.model.Vehicle;
import com.cartrawler.jmgarzo.cartrawler.model.Vendor;

import java.util.ArrayList;

/**
 * Created by jmgarzo on 11/3/2017.
 */


public class SyncTasks {
    private static String LOG_TAG = SyncTasks.class.getSimpleName();


    synchronized public  static void syncDb(Context context) {
        //TODO: Delete values
        ContentResolver contentResolver = context.getContentResolver();
        try {
            //Sync Vendors
            ArrayList<ContentValues> vendorContentValues = getSyncVendors(context);
            if (null != vendorContentValues) {
                //We delete all vendors
                contentResolver.delete(CartrawlerContract.VendorEntry.CONTENT_URI,null,null);
                int operatorsInserted = contentResolver.bulkInsert(CartrawlerContract.VendorEntry.CONTENT_URI,
                        vendorContentValues.toArray(new ContentValues[vendorContentValues.size()]));
                Log.d(LOG_TAG, "Vendor inserted: " + operatorsInserted);
            }

            //Sync Vehicles
            ArrayList<ContentValues> vehicleContentValues = getSyncVehicles(context);
            if (null != vehicleContentValues) {
                //We delete all vendors
                contentResolver.delete(CartrawlerContract.VehicleEntry.CONTENT_URI,null,null);
                int vehicleInserted = contentResolver.bulkInsert(CartrawlerContract.VehicleEntry.CONTENT_URI,
                        vehicleContentValues.toArray(new ContentValues[vehicleContentValues.size()]));
                Log.d(LOG_TAG, "Vehicle inserted: " + vehicleInserted);
            }

            //Sync Rental Core
            ArrayList<ContentValues> rentalCoreContentValues = getSyncRentalCore(context);
            if (null != vendorContentValues) {
                //We delete all vendors
                contentResolver.delete(CartrawlerContract.RentalCoreEntry.CONTENT_URI,null,null);
                int rentalInserted = contentResolver.bulkInsert(CartrawlerContract.RentalCoreEntry.CONTENT_URI,
                        rentalCoreContentValues.toArray(new ContentValues[rentalCoreContentValues.size()]));
                Log.d(LOG_TAG, "Rental Core inserted: " + rentalInserted);
            }

        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
        }
    }

    private static ArrayList<ContentValues> getSyncVendors(Context context) {
        ArrayList<ContentValues> vendorContentValues = null;
        try {
            ArrayList<Vendor> vendorList = NetworkUtils.getVendors(context);
            if (vendorList != null && vendorList.size() > 0) {
                vendorContentValues = new ArrayList<>();
                for (int i = 0; i < vendorList.size(); i++) {
                    Vendor vendor = vendorList.get(i);
                    vendorContentValues.add(vendor.getContentValues());
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
        }
        return vendorContentValues;
    }

    private static ArrayList<ContentValues> getSyncRentalCore(Context context) {
        ArrayList<ContentValues> rentalContentValues = null;
        try {
            ArrayList<RentalCore> rentalCoreList = NetworkUtils.getRentalCore(context);
            if (rentalCoreList != null && rentalCoreList.size() > 0) {
                rentalContentValues = new ArrayList<>();
                for (int i = 0; i < rentalCoreList.size(); i++) {
                    RentalCore rentalCore = rentalCoreList.get(i);
                    rentalContentValues.add(rentalCore.getContentValues());
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
        }
        return rentalContentValues;
    }

    private static ArrayList<ContentValues> getSyncVehicles(Context context) {
        ArrayList<ContentValues> vehiclesContentValues = null;
        try {
            ArrayList<Vehicle> vehicleList = NetworkUtils.getVehicles(context);
            if (vehicleList != null && vehicleList.size() > 0) {
                vehiclesContentValues = new ArrayList<>();
                for (int i = 0; i < vehicleList.size(); i++) {
                    Vehicle vehicle = vehicleList.get(i);
                    vehiclesContentValues.add(vehicle.getContentValues());
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
        }
        return vehiclesContentValues;
    }
}
