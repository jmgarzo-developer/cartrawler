package com.cartrawler.jmgarzo.cartrawler;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;
import com.cartrawler.jmgarzo.cartrawler.Utils.SettingsUtils;
import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;
import com.cartrawler.jmgarzo.cartrawler.model.VehicleVendor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainActivityFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        VehicleAdapter.VehicleAdapterOnClickHandler {

    private static final int ID_RENTAL_CORE_LOADER = 12;
    private static final int ID_VEHICLE_LOADER = 13;

    private TextView mPickUpDate;
    private TextView mPickUpLocation;
    private TextView mReturnDate;
    private TextView mReturnLocation;

    private VehicleAdapter mVehicleAdapter;


    private RecyclerView mRecyclerView;


    public MainActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main_activity, container, false);

        mPickUpDate = rootView.findViewById(R.id.pick_up_date);
        mPickUpLocation = rootView.findViewById(R.id.pick_up_location);
        mReturnDate = rootView.findViewById(R.id.return_date);
        mReturnLocation = rootView.findViewById(R.id.return_location);
        mRecyclerView = rootView.findViewById(R.id.rv_vehicle);

        LinearLayoutManager layoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
        mVehicleAdapter = new VehicleAdapter(getContext(), this);

        mRecyclerView.setAdapter(mVehicleAdapter);


//        Intent intentUpdateDbService = new Intent(getContext(), CartrawlerSyncDbService.class);
//        getActivity().startService(intentUpdateDbService);

        getActivity().getSupportLoaderManager().initLoader(ID_VEHICLE_LOADER, null, this);

        getActivity().getSupportLoaderManager().initLoader(ID_RENTAL_CORE_LOADER, null, this);

        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ID_RENTAL_CORE_LOADER: {
                return new CursorLoader(getContext(),
                        CartrawlerContract.RentalCoreEntry.CONTENT_URI,
                        DbUtils.RENTAL_CORE_COLUMNS,
                        null,
                        null,
                        null);
            }
            case ID_VEHICLE_LOADER: {
                String orderBy = "";
                if (SettingsUtils.isPreferenceSortByPriceAscending(getContext())) {
                    orderBy =CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT + " ASC ";
                } else if (SettingsUtils.isPreferenceSortByPriceDescending(getContext())){
                    orderBy =CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT + " DESC ";
                } else if(SettingsUtils.isPreferenceSortByVendor(getContext())){
                    orderBy = CartrawlerContract.VendorEntry.TABLE_NAME + "." + CartrawlerContract.VendorEntry.NAME;
                }else{
                    orderBy =CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT + " ASC ";

                }
//                CartrawlerContract.VehicleEntry.TABLE_NAME + "." + CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT
                return new CursorLoader(
                        getContext(),
                        CartrawlerContract.VehicleVendorEntry.CONTENT_URI,
                        DbUtils.VEHICLE_VENDOR_COLUMNS,
                        null,
                        null,
                        orderBy
                );
            }
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case ID_RENTAL_CORE_LOADER: {
                if (data != null && data.moveToFirst()) {

                    String sDatePickUp =data.getString(DbUtils.COL_RENTAL_CORE_PICK_UP_DATE_TIME);
                    String sDateReturn = data.getString(DbUtils.COL_RENTAL_CORE_RETURN_DATE_TIME);


                    mPickUpDate.setText(getFormatedString(sDatePickUp));
                    mPickUpLocation.setText(data.getString(DbUtils.COL_RENTAL_CORE_PICK_UP_LOCATION));
                    mReturnDate.setText(getFormatedString(sDateReturn));
                    mReturnLocation.setText(data.getString(DbUtils.COL_RENTAL_CORE_RETURN_LOCATION));
                }
                break;
            }
            case ID_VEHICLE_LOADER: {
                mVehicleAdapter.swapCursor(data);
                break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case ID_VEHICLE_LOADER: {
                mVehicleAdapter.swapCursor(null);
                break;
            }
        }
    }

    @Override
    public void onClick(VehicleVendor vehicleVendor) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(DetailActivityFragment.VEHICLE_INTENT_TAG, vehicleVendor);
        this.startActivity(intent);

    }

    private String getFormatedString(String sDate){

        SimpleDateFormat originFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        GregorianCalendar cal = new GregorianCalendar();

        String formattedDate="";

        try {
            cal.setTime(originFormat.parse(sDate));
            java.util.Date date = cal.getTime();
            formattedDate = newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
}
