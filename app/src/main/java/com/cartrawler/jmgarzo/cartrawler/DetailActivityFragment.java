package com.cartrawler.jmgarzo.cartrawler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cartrawler.jmgarzo.cartrawler.model.VehicleVendor;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    public static final String VEHICLE_INTENT_TAG = "vehicle_tag";

    private VehicleVendor mVehicleVendor;

    @BindView(R.id.iv_car_image)
    ImageView mImgCar;
    @BindView(R.id.tv_vendor)
    TextView mTxtVendor;
    @BindView(R.id.tv_make_model)
    TextView mTxtMakeModel;
    @BindView(R.id.tv_fuel)
    TextView mTxtFuel;
    @BindView(R.id.tv_transmission)
    TextView mTxtTransmission;
    @BindView(R.id.tv_air_condition)
    TextView mTxtAirCondition;
    @BindView(R.id.tv_doors)
    TextView mTxtDoors;
    @BindView(R.id.tv_passenger)
    TextView mTxtPasassenger;
    @BindView(R.id.tv_baggage)
    TextView mTxtBaggage;
    @BindView(R.id.tv_price)
    TextView mTxtPrice;
    @BindView(R.id.tv_currency)
    TextView mTxtCurrency;


    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_detail, container, false);

        ButterKnife.bind(this,viewRoot);


        Intent intent = getActivity().getIntent();
        if (null != intent) {
            mVehicleVendor = intent.getParcelableExtra(DetailActivityFragment.VEHICLE_INTENT_TAG);
            loadData();
        }

        return viewRoot;
    }

    private void loadData(){
        Picasso.with(getActivity())
                .load(mVehicleVendor.getPictureUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.ic_broken_image_black_48px)
                .tag(getActivity())
                .into(mImgCar);

        mTxtVendor.setText(mVehicleVendor.getVendorName());
        mTxtMakeModel.setText(mVehicleVendor.getVehicleMakeModel());
        mTxtFuel.setText(mVehicleVendor.getFuelType());
        mTxtTransmission.setText(mVehicleVendor.getTransmissionType());
        if(mVehicleVendor.getAirConditionInd().equalsIgnoreCase("true")){
            mTxtAirCondition.setVisibility(View.VISIBLE);
        }else{
            mTxtAirCondition.setVisibility(View.GONE);
        }
        mTxtDoors.setText(mVehicleVendor.getDoorCount());
        mTxtPasassenger.setText(mVehicleVendor.getPassengerQuantity());
        mTxtBaggage.setText(mVehicleVendor.getBaggageQuantity());
        mTxtPrice.setText(Double.toString(mVehicleVendor.getRateTotalAmount()));
        mTxtCurrency.setText(mVehicleVendor.getCurrencyCode());
    }

}
