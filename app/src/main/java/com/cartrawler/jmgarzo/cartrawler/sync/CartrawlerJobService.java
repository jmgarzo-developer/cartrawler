package com.cartrawler.jmgarzo.cartrawler.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class CartrawlerJobService extends JobService {

    private static AsyncTask<Void, Void, Void> mFetchVehicleTask;

    @Override
    public boolean onStartJob(final JobParameters params) {

        mFetchVehicleTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Context context = getApplicationContext();
                SyncTasks.syncDb(context);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                jobFinished(params, false);
            }
        };
        mFetchVehicleTask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
