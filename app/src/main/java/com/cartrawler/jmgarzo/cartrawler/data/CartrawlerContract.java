package com.cartrawler.jmgarzo.cartrawler.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by jmgarzo on 11/2/2017.
 */

public class CartrawlerContract {

    public static final String CONTENT_AUTHORITY = "com.cartrawler.jmgarzo.cartrawler";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_VENDOR = "vendor";
    public static final String PATH_VEHICLE = "vehicle";
    public static final String PATH_RENTAL_CORE = "rental_core";
    public static final String PATH_VEHICLE_VENDOR = "vehicle_vendor";


    public static final class VendorEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_VENDOR).build();

        public static final String TABLE_NAME = "vendor";
        public static final String _ID = "_id";
        public static final String CODE = "code";
        public static final String NAME = "name";

        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;

    }

    public static final class VehicleEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_VEHICLE).build();

        public static final String TABLE_NAME = "vehicle";
        public static final String _ID = "_id";
        public static final String VENDOR = "vendor";
        public static final String STATUS = "status";
        public static final String AIR_CONDITION_IND = "air_condition_ind";
        public static final String TRANSMISSION_TYPE = "transmission_type";
        public static final String FUEL_TYPE = "fuel_type";
        public static final String DRIVE_TYPE = "drive_type";
        public static final String PASSENGER_QUANTITY = "passenger_quantity";
        public static final String BAGGAGE_QUANTITY = "baggage_quantity";
        public static final String CODE = "code";
        public static final String CODE_CONTEXT = "code_context";
        public static final String DOOR_COUNT =  "door_count";
        public static final String VEHICLE_MAKE_MODEL = "vehicle_make_model";
        public static final String PICTURE_URL = "picture_url";
        public static final String RATE_TOTAL_AMOUNT = "rate_total_amount";
        public static final String ESTIMATED_TOTAL_AMOUNT = "estimated_total_amount";
        public static final String CURRENCY_CODE = "currency_code";

        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;


    }

    public static final class RentalCoreEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_RENTAL_CORE).build();

        public static final String TABLE_NAME = "rental_core";
        public static final String _ID = "_id";
        public static final String PICK_UP_DATE_TIME = "pick_up_date_time";
        public static final String RETURN_DATE_TIME = "return_date_time";
        public static final String PICK_UP_LOCATION = "pick_up_location";
        public static final String RETURN_LOCATION = "return_location";

        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;
    }

    public static final class VehicleVendorEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_VEHICLE_VENDOR).build();

        public static Uri buildVehicleVendor() {
            return CONTENT_URI.buildUpon().build();
        }

    }

}
