package com.cartrawler.jmgarzo.cartrawler.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jmgarzo on 11/2/2017.
 */

public class CartrawlerDBHelper  extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Cartrawler.db";

    private static final String LOG_TAG = CartrawlerDBHelper.class.getSimpleName();

    private static final String SQL_CREATE_VENDOR_TABLE =
            "CREATE TABLE " + CartrawlerContract.VendorEntry.TABLE_NAME + " ( " +
                    CartrawlerContract.VendorEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    CartrawlerContract.VendorEntry.CODE + " TEXT NOT NULL, " +
                    CartrawlerContract.VendorEntry.NAME + " TEXT NOT NULL " +
                    " );";

    private static final String SQL_CREATE_VEHICLE_TABLE =
            "CREATE TABLE " + CartrawlerContract.VehicleEntry.TABLE_NAME + " ( " +
                    CartrawlerContract.VehicleEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    CartrawlerContract.VehicleEntry.VENDOR + " INTEGER NOT NULL, " +
                    CartrawlerContract.VehicleEntry.STATUS + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.AIR_CONDITION_IND + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.TRANSMISSION_TYPE + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.FUEL_TYPE + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.DRIVE_TYPE + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.PASSENGER_QUANTITY + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.BAGGAGE_QUANTITY + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.CODE + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.CODE_CONTEXT + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.DOOR_COUNT + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.VEHICLE_MAKE_MODEL + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.PICTURE_URL + " TEXT NOT NULL, " +
                    CartrawlerContract.VehicleEntry.RATE_TOTAL_AMOUNT + " REAL NOT NULL, " +
                    CartrawlerContract.VehicleEntry.ESTIMATED_TOTAL_AMOUNT + " REAL NOT NULL, " +
                    CartrawlerContract.VehicleEntry.CURRENCY_CODE + " TEXT NOT NULL, " +
                    " FOREIGN KEY (" + CartrawlerContract.VehicleEntry.VENDOR + ") REFERENCES " +
                    CartrawlerContract.VendorEntry.TABLE_NAME + " (" + CartrawlerContract.VendorEntry._ID + ") ON DELETE CASCADE " +
                    ");";

    private static  final String SQL_CREATE_RENTAL_CORE_TABLE =
            "CREATE TABLE " + CartrawlerContract.RentalCoreEntry.TABLE_NAME + " ( " +
                    CartrawlerContract.RentalCoreEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    CartrawlerContract.RentalCoreEntry.PICK_UP_DATE_TIME + " TEXT NOT NULL, " +
                    CartrawlerContract.RentalCoreEntry.RETURN_DATE_TIME + " TEXT NOT NULL, " +
                    CartrawlerContract.RentalCoreEntry.PICK_UP_LOCATION + " TEXT NOT NULL, " +
                    CartrawlerContract.RentalCoreEntry.RETURN_LOCATION + " TEXT NOT NULL " +
                    " );";


    public CartrawlerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + CartrawlerContract.RentalCoreEntry.TABLE_NAME );
        db.execSQL("DROP TABLE IF EXISTS " + CartrawlerContract.VehicleEntry.TABLE_NAME );
        db.execSQL("DROP TABLE IF EXISTS " + CartrawlerContract.VendorEntry.TABLE_NAME );

        db.execSQL(SQL_CREATE_RENTAL_CORE_TABLE);
        db.execSQL(SQL_CREATE_VENDOR_TABLE);

        db.execSQL(SQL_CREATE_VEHICLE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        db.execSQL(SQL_CREATE_VENDOR_TABLE);
        db.execSQL(SQL_CREATE_VEHICLE_TABLE);
        db.execSQL(SQL_CREATE_RENTAL_CORE_TABLE);
    }
}
