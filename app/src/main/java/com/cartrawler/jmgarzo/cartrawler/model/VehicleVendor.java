package com.cartrawler.jmgarzo.cartrawler.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class VehicleVendor implements Parcelable {

    private long id;
    private String status;
    private String airConditionInd;
    private String transmissionType;
    private String fuelType;
    private String driveType;
    private String passengerQuantity;
    private String baggageQuantity;
    private String vehicleCode;
    private String codeContext;
    private String doorCount;
    private String vehicleMakeModel;
    private String pictureUrl;
    private Double rateTotalAmount;
    private Double estimatedTotalAmount;
    private String currencyCode;
    private String vendorCode;
    private String vendorName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAirConditionInd() {
        return airConditionInd;
    }

    public void setAirConditionInd(String airConditionInd) {
        this.airConditionInd = airConditionInd;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getPassengerQuantity() {
        return passengerQuantity;
    }

    public void setPassengerQuantity(String passengerQuantity) {
        this.passengerQuantity = passengerQuantity;
    }

    public String getBaggageQuantity() {
        return baggageQuantity;
    }

    public void setBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
    }

    public String getVehicleCode() {
        return vehicleCode;
    }

    public void setVehicleCode(String vehicleCode) {
        this.vehicleCode = vehicleCode;
    }

    public String getCodeContext() {
        return codeContext;
    }

    public void setCodeContext(String codeContext) {
        this.codeContext = codeContext;
    }

    public String getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(String doorCount) {
        this.doorCount = doorCount;
    }

    public String getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    public void setVehicleMakeModel(String vehicleMakeModel) {
        this.vehicleMakeModel = vehicleMakeModel;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Double getRateTotalAmount() {
        return rateTotalAmount;
    }

    public void setRateTotalAmount(Double rateTotalAmount) {
        this.rateTotalAmount = rateTotalAmount;
    }

    public Double getEstimatedTotalAmount() {
        return estimatedTotalAmount;
    }

    public void setEstimatedTotalAmount(Double estimatedTotalAmount) {
        this.estimatedTotalAmount = estimatedTotalAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }


    public void cursorToVehicleVendor(Cursor cursor,int position){

        if(cursor!= null && cursor.moveToPosition(position)) {
            id = cursor.getLong(DbUtils.COL_VEHICLE_ID);
            status = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_STATUS);
            airConditionInd = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_AIR_CONDITION_IND);
            transmissionType = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_TRANSMISSION_TYPE);
            fuelType = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_FUEL_TYPE);
            driveType = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_DRIVE_TYPE);
            passengerQuantity = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_PASSENGER_QUANTITY);
            baggageQuantity = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_BAGGAGE_QUANTITY);
            vehicleCode = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_VEHICLE_CODE);
            codeContext = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_CODE_CONTEXT);
            doorCount = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_DOOR_COUNT);
            vehicleMakeModel = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_VEHICLE_MAKE_MODEL);
            pictureUrl = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_PICTURE_URL);
            rateTotalAmount = cursor.getDouble(DbUtils.COL_VEHICLE_VENDOR_RATE_TOTAL_AMOUNT);
            estimatedTotalAmount = cursor.getDouble(DbUtils.COL_VEHICLE_VENDOR_ESTIMATED_TOTAL_AMOUNT);
            currencyCode = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_CURRENCY_CODE);
            vendorName = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_NAME);
            vendorCode = cursor.getString(DbUtils.COL_VEHICLE_VENDOR_VENDOR_CODE);
        }

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.status);
        dest.writeString(this.airConditionInd);
        dest.writeString(this.transmissionType);
        dest.writeString(this.fuelType);
        dest.writeString(this.driveType);
        dest.writeString(this.passengerQuantity);
        dest.writeString(this.baggageQuantity);
        dest.writeString(this.vehicleCode);
        dest.writeString(this.codeContext);
        dest.writeString(this.doorCount);
        dest.writeString(this.vehicleMakeModel);
        dest.writeString(this.pictureUrl);
        dest.writeValue(this.rateTotalAmount);
        dest.writeValue(this.estimatedTotalAmount);
        dest.writeString(this.currencyCode);
        dest.writeString(this.vendorCode);
        dest.writeString(this.vendorName);
    }

    public VehicleVendor() {
    }

    protected VehicleVendor(Parcel in) {
        this.id = in.readLong();
        this.status = in.readString();
        this.airConditionInd = in.readString();
        this.transmissionType = in.readString();
        this.fuelType = in.readString();
        this.driveType = in.readString();
        this.passengerQuantity = in.readString();
        this.baggageQuantity = in.readString();
        this.vehicleCode = in.readString();
        this.codeContext = in.readString();
        this.doorCount = in.readString();
        this.vehicleMakeModel = in.readString();
        this.pictureUrl = in.readString();
        this.rateTotalAmount = (Double) in.readValue(Double.class.getClassLoader());
        this.estimatedTotalAmount = (Double) in.readValue(Double.class.getClassLoader());
        this.currencyCode = in.readString();
        this.vendorCode = in.readString();
        this.vendorName = in.readString();
    }

    public static final Parcelable.Creator<VehicleVendor> CREATOR = new Parcelable.Creator<VehicleVendor>() {
        @Override
        public VehicleVendor createFromParcel(Parcel source) {
            return new VehicleVendor(source);
        }

        @Override
        public VehicleVendor[] newArray(int size) {
            return new VehicleVendor[size];
        }
    };
}
