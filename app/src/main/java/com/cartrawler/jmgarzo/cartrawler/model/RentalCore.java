package com.cartrawler.jmgarzo.cartrawler.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.cartrawler.jmgarzo.cartrawler.Utils.DbUtils;
import com.cartrawler.jmgarzo.cartrawler.data.CartrawlerContract;

/**
 * Created by jmgarzo on 11/3/2017.
 */

public class RentalCore implements Parcelable {

    private long id;
    private String pickUpDateTime;
    private String returnDateTime;
    private String pickUpLocation;
    private String returnLocation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPickUpDateTime() {
        return pickUpDateTime;
    }

    public void setPickUpDateTime(String pickUpDateTime) {
        this.pickUpDateTime = pickUpDateTime;
    }

    public String getReturnDateTime() {
        return returnDateTime;
    }

    public void setReturnDateTime(String returnDateTime) {
        this.returnDateTime = returnDateTime;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getReturnLocation() {
        return returnLocation;
    }

    public void setReturnLocation(String returnLocation) {
        this.returnLocation = returnLocation;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(CartrawlerContract.RentalCoreEntry.PICK_UP_DATE_TIME, getPickUpDateTime());
        contentValues.put(CartrawlerContract.RentalCoreEntry.RETURN_DATE_TIME, getReturnDateTime());
        contentValues.put(CartrawlerContract.RentalCoreEntry.PICK_UP_LOCATION, getPickUpLocation());
        contentValues.put(CartrawlerContract.RentalCoreEntry.RETURN_LOCATION, getReturnLocation());

        return contentValues;
    }

    public void cursorToVendor(Cursor cursor) {
        id = cursor.getLong(DbUtils.COL_RENTAL_CORE_ID);
        pickUpDateTime = cursor.getString(DbUtils.COL_RENTAL_CORE_PICK_UP_DATE_TIME);
        returnDateTime = cursor.getString(DbUtils.COL_RENTAL_CORE_RETURN_DATE_TIME);
        pickUpLocation = cursor.getString(DbUtils.COL_RENTAL_CORE_PICK_UP_LOCATION);
        returnLocation = cursor.getString(DbUtils.COL_RENTAL_CORE_RETURN_LOCATION);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.pickUpDateTime);
        dest.writeString(this.returnDateTime);
        dest.writeString(this.pickUpLocation);
        dest.writeString(this.returnLocation);
    }

    public RentalCore() {
    }

    protected RentalCore(Parcel in) {
        this.id = in.readLong();
        this.pickUpDateTime = in.readString();
        this.returnDateTime = in.readString();
        this.pickUpLocation = in.readString();
        this.returnLocation = in.readString();
    }

    public static final Parcelable.Creator<RentalCore> CREATOR = new Parcelable.Creator<RentalCore>() {
        @Override
        public RentalCore createFromParcel(Parcel source) {
            return new RentalCore(source);
        }

        @Override
        public RentalCore[] newArray(int size) {
            return new RentalCore[size];
        }
    };
}
